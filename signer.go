package main

import (
	"sort"
	"strconv"
	"sync"
)

var mutex sync.Mutex

func SingleHash(in chan interface{}, out chan interface{}) {
	var wg sync.WaitGroup
	defer wg.Wait()
	for data:= range in {
		wg.Add(1)

		go func(data interface{}, out chan interface{}) {
			defer wg.Done()
			channel := make(chan string)

			mutex.Lock()
			md5 := DataSignerMd5(strconv.Itoa(data.(int)))
			mutex.Unlock()

			go func(channel chan string, data string){
				channel <- DataSignerCrc32(data)
			}(channel, strconv.Itoa(data.(int)))

			crc32withMd5 := DataSignerCrc32(md5)
			crc32string := <-channel

			out <- crc32string+ "~" + crc32withMd5
			close(channel)

		}(data, out)
	}
}



func MultiHash(in chan interface{}, out chan interface{}) {
	var wg sync.WaitGroup

	wg.Wait()
}

func combineResults(strings []string) string {
	var res string
	sort.Strings(strings)
	for _, str := range strings{
		if res != "" {
			res = res + "_" + str
		} else {
			res = str
		}
	}
	return res
}

func CombineResults(in chan interface{}, out chan interface{}) {
	var array []string
	for data := range in {
		array = append(array, data.(string))
	}
	out<-combineResults(array)
}

func ExecutePipeline(jobs ...job) {
	var waitGroup sync.WaitGroup
	in := make(chan interface{}, 7)
	out := make(chan interface{}, 7)
	for _, j := range jobs {
		waitGroup.Add(1)
		go func(j job, in chan interface{}, out chan interface{}, wg *sync.WaitGroup) {
			defer wg.Done()
			defer close(out)
			j(in, out)
		}(j, in, out, &waitGroup)
	}
	waitGroup.Wait()
}